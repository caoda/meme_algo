__author__ = 'dacao'

import sys
import json
import csv
import datetime
import unicodedata
import operator
import numpy
import pymongo

# import matplotlib
# import matplotlib.pyplot as plt
# from matplotlib.pyplot import plot, draw, show


import functions as func
import quantizing
from quantizing import quantize_action_elapsed_times_based_on_duration as quantize
# import boxplots

m_video = "VideoMeasurement"
m_pdf = 'PDFMeasurement'
m_lifecycle = 'LifeCycleMeasurement'
m_article = 'ArticleViewPositionMeasurement'
m_slide = 'SlidePlaybackEventMeasurement'
m_window = 'WindowLayoutMeasurement'
m_navi = 'NavigationMeasurement'
m_slideChanged = 'SlideChangedMeasurement'
m_slideCompleted = 'SlideCompletedMeasurement'



DB_name = 'prod_backend'

options = {
             'NAME': 'prod_backend',
             'HOST': 'ec2-34-197-131-74.compute-1.amazonaws.com', #,ec2-34-192-210-175.compute-1.amazonaws.com,ec2-34-197-47-228.compute-1.amazonaws.com'
             'PORT': 27017,
             'USER': 'prod',
             'PASSWORD': '|CtNtXK!D46vKXB',
             'REPLICASET': 'prod-mongo-rs-raw',
             'CONNECTION': None
        }

def do_connect_mongo(options):

    if options['REPLICASET'] == "":
        connection = pymongo.MongoClient(options['HOST'], options['PORT'])
    else:
        connection = pymongo.MongoClient(options['HOST'], options['PORT'], replicaset=options['REPLICASET'])
    connection[options['NAME']].authenticate(options['USER'], options['PASSWORD'])

    return connection


def prepare_for_main(course_id, collection_raw_events):

    events = list(  collection_raw_events.find( {"course": course_id} ).sort('timestamp')  )

    chap_names = []
    for evt in events:
        if 'chapter' in evt and evt['chapter'] not in chap_names:
            chap_names.append(evt['chapter'])
    chap_names = sorted(chap_names)

    return events, chap_names


def main(courseID):

    CONNECTION = do_connect_mongo(options)
    collection_raw_events = CONNECTION[DB_name]['measure_base']

    all_events, chap_names = prepare_for_main(courseID, collection_raw_events)
    chap_names = ['init'] + chap_names


    uv = func.get_uv_dict(all_events)
    uv = func.make_valid(uv)
    users = sorted(uv.keys())

    dict_user_ActionSequences = {}

    for user in users:
        actionSequences = []
        all_evts_this_user = uv[user]

        ref_slideIndex = -1
        ref_chap = 'init'
        just_did_lc = False

        for i, e in enumerate(all_evts_this_user):
            chap = e.get('chapter', 'none')
            slideIndex = e.get('slideIndex', -1)
            m_type = e.get('m_type', 'none')
            t = e['timestamp']

            if chap != "none" and chap != ref_chap:
                new_ind = chap_names.index(chap)
                old_ind = chap_names.index(ref_chap)
                if new_ind - old_ind > 0:
                    act = 'chapF'  # +  str(int(new_ind-old_ind))
                else:
                    act = 'chapB'
                act = 'chap' + str(int(new_ind-old_ind))
                actionSequences.append({'chapter':chap, 'timestamp':t, 'action':act})
                ref_chap = chap


            if slideIndex != ref_slideIndex:
                if ref_slideIndex == -1 or slideIndex == -1:
                    ref_slideIndex = slideIndex
                else:
                    if slideIndex > ref_slideIndex:
                        act = 'SlideF'
                    else:
                        act = 'SlideB'
                    # actionSequences.append({'chapter':chap, 'timestamp':t, 'action':act, 'slideIndex':slideIndex, 'slideID':e['slide']})
                    ref_slideIndex = slideIndex


            if m_type == m_lifecycle:
                # if e.get('action', 'none') == 'backgroundTab' and lc_state == 'foregroundTab':
                #     lc_state = 'backgroundTab'
                if e.get('action', 'none') == 'foregroundTab' and all_evts_this_user[i-1].get('action', 'none') == 'backgroundTab':
                    # lc_state = 'foregroundTab'
                    t0 = all_evts_this_user[i-1]['timestamp']
                    if just_did_lc == True:
                        actionSequences[-1]['out_time'] += t - t0
                    else:
                        actionSequences.append({'chapter':ref_chap, 'timestamp':t0, 'action':'W', 'out_time':t - t0})
                    just_did_lc = True

            if e.get('action', 'none') not in ['foregroundTab', 'backgroundTab']:
                just_did_lc = False


        for act in actionSequences:
            if act.get('out_time', 'none') != 'none':
                out_time = act.get('out_time')
                if out_time <= 15:
                    act['action'] = 'W_1'
                elif out_time > 15:
                    act['action'] = 'W_2'
        dict_user_ActionSequences[user] = actionSequences

    # with open('PBP_data2/users_slideIDs_sequences_%s.json' %courseID, 'w') as fp:
    #     json.dump(actions_dict_user_slideID_sequences, fp)

    return dict_user_ActionSequences, chap_names




if __name__ == '__main__':
    debug = False
    debug_slide_id = u''
    debug_user = u''

    courseID = 'PBP_WLC_303_4'
    main(courseID)

