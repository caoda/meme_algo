__author__ = 'dacao'
import time
import sys
import json
import csv
import datetime
import unicodedata
import operator
import numpy as np
import pymongo

m_video = "VideoMeasurement"
m_pdf = 'PDFMeasurement'
m_lifecycle = 'LifeCycleMeasurement'
m_article = 'ArticleViewPositionMeasurement'
m_slide = 'SlidePlaybackEventMeasurement'
m_window = 'WindowLayoutMeasurement'
m_navi = 'NavigationMeasurement'
m_slideChanged = 'SlideChangedMeasurement'
m_slideCompleted = 'SlideCompletedMeasurement'



DB_name = 'prod_backend'

options = {
             'NAME': 'prod_backend',
             'HOST': 'ec2-34-197-131-74.compute-1.amazonaws.com', #,ec2-34-192-210-175.compute-1.amazonaws.com,ec2-34-197-47-228.compute-1.amazonaws.com'
             'PORT': 27017,
             'USER': 'prod',
             'PASSWORD': '|CtNtXK!D46vKXB',
             'REPLICASET': 'prod-mongo-rs-raw',
             'CONNECTION': None
        }

def do_connect_mongo(options):

    if options['REPLICASET'] == "":
        connection = pymongo.MongoClient(options['HOST'], options['PORT'])
    else:
        connection = pymongo.MongoClient(options['HOST'], options['PORT'], replicaset=options['REPLICASET'])
    connection[options['NAME']].authenticate(options['USER'], options['PASSWORD'])

    return connection


# def do_connect_mongo():
#     # try:
#     connection = pymongo.MongoClient(host= host_id, port=27017)
#     connection[DB_name].authenticate(DB_user, DB_password)
#     # except (SystemError, StandardError, pymongo.errors.OperationFailure), connection_error:
#     #     raise ValueError('Can`t connect to %s' % 'mongo prod_backend', connection_error)
#     return connection
#


CONNECTION = do_connect_mongo(options)
collection_raw_events = CONNECTION[DB_name]['measure_base']
# collection_HEAT_MAPS = CONNECTION[DB_name]['user_content_interaction_interval_units']
# [u'measure_base', u'system.indexes', u'time_spent_units', u'user_content_interaction_interval_units', u'users_in_course_and_company']


THRES_skip = 5


'''/////////////////////////////////////////////////////////////////////////////////////////////////////////////////'''


def prepare_stuff(collection_raw_events, course_id):
    pdf_measurements = list(  collection_raw_events.find( {"course": course_id, 'm_type': m_pdf} )  )
    dict_pdfID_page_expectedTime = pdf_expected_times_for_a_course(course_id)
    dict_chapter_pdfID = {}
    dict_pdfID_pdfTotalPages = {}
    for evt in pdf_measurements:
        if evt['chapter'] not in dict_chapter_pdfID:
            dict_chapter_pdfID[ evt['chapter'] ] = [evt['content']]
        else:
            if evt['content'] not in dict_chapter_pdfID[ evt['chapter'] ]:
                dict_chapter_pdfID[ evt['chapter'] ].append( evt['content'] )

        if evt['content'] not in dict_pdfID_pdfTotalPages:
            dict_pdfID_pdfTotalPages[ evt['content'] ] = { evt['totalPages'] : 1 }
        else:
            if evt['totalPages'] not in dict_pdfID_pdfTotalPages[ evt['content'] ]:
                dict_pdfID_pdfTotalPages[ evt['content'] ][ evt['totalPages']  ] = 1
            else:
                dict_pdfID_pdfTotalPages[ evt['content'] ][ evt['totalPages']  ] += 1

    for pdfID in dict_pdfID_pdfTotalPages:
        total_pages = 0
        max_count = 0
        for candidate in dict_pdfID_pdfTotalPages[pdfID]:
            if dict_pdfID_pdfTotalPages[pdfID][candidate] > max_count:
                max_count = dict_pdfID_pdfTotalPages[pdfID][candidate]
                total_pages = candidate
        dict_pdfID_pdfTotalPages[pdfID] = total_pages


    for pdfID in dict_pdfID_page_expectedTime:
        if pdfID not in dict_pdfID_pdfTotalPages:
            continue
        if len( dict_pdfID_page_expectedTime[pdfID] ) < dict_pdfID_pdfTotalPages[pdfID]:
            for i in range(0, dict_pdfID_pdfTotalPages[pdfID]):
                if i not in dict_pdfID_page_expectedTime[pdfID]:
                    dict_pdfID_page_expectedTime[pdfID][i] = 10


    for chap in dict_chapter_pdfID:
        if len(dict_chapter_pdfID[chap]) > 1:
            print('mismatch',  chap, dict_chapter_pdfID[chap])

        dict_chapter_pdfID[chap] = dict_chapter_pdfID[chap][0]


    return dict_chapter_pdfID, dict_pdfID_pdfTotalPages, dict_pdfID_page_expectedTime



def pdf_expected_times_for_a_course(courseID):

    fname = 'pdf_words_in_courses/%s_all_PDFs_words.json'%courseID
    in_file = open(fname,"r")
    words_in_PDFs_of_a_course = json.load(in_file) # words_in_PDFs_of_a_course[contentID][page_name] = [all words in the page]
    in_file.close()

    dict_pdfID_page_expectedTime = {}
    for pdf_id in words_in_PDFs_of_a_course:
        dict_pdfID_page_expectedTime[pdf_id] = {}
        for pageName in words_in_PDFs_of_a_course[pdf_id]:
            a = pageName.index('[')
            b = pageName.index(']')
            pageNumber = int(pageName[a+1:b]) + 1
            dict_pdfID_page_expectedTime[pdf_id][pageNumber] = len( words_in_PDFs_of_a_course[pdf_id][pageName] ) / 3.0

    return dict_pdfID_page_expectedTime


def find_pdf_motif_raw_sequences(events):

    current_pdf_page = 'none'
    ref_time = 'none'

    raw_seq = []

    # find the first pdf event if possible
    first_pdf_event_exist = False
    for i, evt in enumerate(events):
        if evt['m_type'] == m_pdf:
            first_pdf_event_exist = True
            first_ind = i
            current_pdf_page = evt['page']
            ref_time = evt['timestamp']
            break

    if first_pdf_event_exist:
        events = events[first_ind:]

        # todo: delete
        # # ref = 'none'
        # for evt in events:
        #     if evt['m_type'] == m_pdf:
        #         print evt['page'],
        #         # if evt['page'] - ref != 1:
        #         #     print(evt['page'] - ref)
        # print('/')

        for i, evt in enumerate(events):
            if evt['m_type'] == m_pdf:
                if current_pdf_page != 'idle':
                    act = {'page': current_pdf_page, 'time_elapsed': evt['timestamp'] - ref_time, 'timestamp': evt['timestamp'], 'chapter': evt['chapter']}
                    raw_seq.append(act)
                current_pdf_page = evt['page']
                ref_time = evt['timestamp']

            elif evt['m_type'] in [m_navi, m_lifecycle] and evt['action'] in ['open', 'close']:

                if current_pdf_page != 'idle':
                    act = {'page': current_pdf_page, 'time_elapsed': evt['timestamp'] - ref_time, 'timestamp': evt['timestamp'], 'chapter': 'nav_lc_open_close'}
                    raw_seq.append(act)
                current_pdf_page = 'idle'
                ref_time = 'idle'

            elif evt['m_type'] in [m_slide, m_video, m_article]:
                if current_pdf_page != 'idle':
                    act = {'page': current_pdf_page, 'time_elapsed': evt['timestamp'] - ref_time, 'timestamp': evt['timestamp'], 'chapter': evt['chapter']}
                    raw_seq.append(act)
                current_pdf_page = 'idle'
                ref_time = 'idle'

        # handle last pdf situation

    # filter out those with time elapsed that are too short
    raw_seq = [act for act in raw_seq if act['time_elapsed'] > 1.0]

    return raw_seq


def analyze_raw_seq_to_act_time_seq(raw_seq, pageNumbers_expectedTimes):

    action_list = []

    seq = []
    for i, item in enumerate(raw_seq):
        if item['time_elapsed'] > THRES_skip:
            seq.append(item)

    for i, item in enumerate(seq):
        page = item['page']
        time_elapsed = item['time_elapsed']
        timestamp = item['timestamp']
        chapter = item['chapter']

        if i > 0 and page == seq[i-1]['page']:
                action_list[-1]['Re'] += time_elapsed
        else:
            action_list.append( {'Re': time_elapsed, 'page': page, 'timestamp': timestamp, 'chapter': chapter} )

        if i < len(seq)-1:
            skipped = seq[i+1]['page'] - page
            if skipped != 1:
                if skipped > 1:
                    action_list.append( {'Sf': skipped, 'page': page, 'timestamp': timestamp, 'chapter': chapter})
                elif skipped < 0:
                    action_list.append( {'Sb': abs(skipped), 'page': page, 'timestamp': timestamp, 'chapter': chapter})

    return action_list





def which_quartile(value, q1, q2, q3):

    if value <= q1:
        return 1
    elif q1 < value <= q2:
        return 2
    elif q2 < value <= q3:
        return 3
    else:
        return 4


def which_quartile_2(value,q2):
    if value <= q2:
        return 1
    else:
        return 2



def quantize_raw_elapsedTime(dict_user_actTimeSeq, chapters):

    for ch in chapters:
        times_read = []
        times_pause = []
        times_skip = []
        for u in dict_user_actTimeSeq:
            if ch not in dict_user_actTimeSeq[u]:
                continue
            seq = dict_user_actTimeSeq[u][ch]
            for act in seq:
                if act.keys() == ['Re']:
                    times_read.append(act['Re'])
                if act.keys() == ['Sf']:
                    times_skip.append(act['Sf'])
                if act.keys() == ['Sb']:
                    times_skip.append(act['Sb'])

        if len(times_read) > 0:
            read_q1 = np.percentile(times_read, 25)
            read_q2 = np.percentile(times_read, 50)
            read_q3 = np.percentile(times_read, 75)

        if len(times_skip) > 0:
            skip_q1 = np.percentile(times_skip, 25)
            skip_q2 = np.percentile(times_skip, 50)
            skip_q3 = np.percentile(times_skip, 75)

        for u in dict_user_actTimeSeq:
            if ch not in dict_user_actTimeSeq[u]:
                continue
            seq = dict_user_actTimeSeq[u][ch]
            for act in seq:
                if act.keys() == ['Re']:
                    act['Re'] = which_quartile(act['Re'], read_q1, read_q2, read_q3)
                elif act.keys() == ['Sf']:
                    act['Sf'] = which_quartile(act['Sf'], skip_q1, skip_q2, skip_q3)
                elif act.keys() == ['Sb']:
                    act['Sb'] = which_quartile(act['Sb'], skip_q1, skip_q2, skip_q3)

    return dict_user_actTimeSeq



def quantize_raw_elapsedTime_2(dict_user_actTimeSeq, chapters):

    for ch in chapters:
        times_read = []
        times_pause = []
        times_skip = []
        for u in dict_user_actTimeSeq:
            if ch not in dict_user_actTimeSeq[u]:
                continue
            seq = dict_user_actTimeSeq[u][ch]
            for act in seq:
                if 'Re' in act.keys():
                    times_read.append(act['Re'])
                if 'Sf' in act.keys():
                    times_skip.append(act['Sf'])
                if 'Sb' in act.keys():
                    times_skip.append(act['Sb'])

        if len(times_read) > 0:
            read_q2 = np.percentile(times_read, 50)

        if len(times_skip) > 0:
            skip_q2 = np.percentile(times_skip, 50)

        for u in dict_user_actTimeSeq:
            if ch not in dict_user_actTimeSeq[u]:
                continue
            seq = dict_user_actTimeSeq[u][ch]
            for act in seq:
                if 'Re' in act.keys():
                    act['Re'] = which_quartile_2(act['Re'], read_q2)
                elif 'Sf' in act.keys():
                    act['Sf'] = which_quartile_2(act['Sf'], skip_q2)
                elif 'Sb' in act.keys():
                    act['Sb'] = which_quartile_2(act['Sb'], skip_q2)

    return dict_user_actTimeSeq





def main(course_id, print_debug, debug_slide_id, debug_user):

    collection_users = CONNECTION[DB_name]['users_in_course_and_company']
    collection_raw_events = CONNECTION[DB_name]['measure_base']

    # todo: get dict_chapter_pdfID   dict_pdfID_pageNumber_expectedTime  dict_pdfID_pdfTotalPages
    dict_chapter_pdfID, dict_pdfID_pdfTotalPages, \
    dict_pdfID_page_expectedTime = prepare_stuff(collection_raw_events, course_id)


    user_list = list(  collection_users.find( {"course": course_id} )  )

    all_users = []
    for item in user_list:
        for u in item['users']:
            if u not in all_users:
                all_users.append(u)

    dict_user_chapter_actTimeSeq = {}

    for u in all_users:
        print(u)
        raw_events = list(  collection_raw_events.find( {"course": course_id, 'user': u} ).sort('timestamp')  )

        dict_chapter_events = {}
        for evt in raw_events:
            if 'chapter' in evt:
                if evt['chapter'] not in dict_chapter_events:
                    dict_chapter_events[ evt['chapter'] ] = [evt]
                else:
                    dict_chapter_events[ evt['chapter'] ].append( evt )

        dict_chapter_raw_sequences = {}
        for ch in dict_chapter_events:
            events = dict_chapter_events[ch]
            dict_chapter_raw_sequences[ch] = find_pdf_motif_raw_sequences(events)


        dict_chapter_act_time_seq = {}
        for ch in dict_chapter_events:
            if ch not in dict_chapter_pdfID:
                continue
            raw_seq = dict_chapter_raw_sequences[ch]
            pdfID = dict_chapter_pdfID[ch]
            if pdfID not in dict_pdfID_page_expectedTime:
                continue
            pageNumbers_expectedTimes = dict_pdfID_page_expectedTime[pdfID]
            act_time_seq = analyze_raw_seq_to_act_time_seq(raw_seq, pageNumbers_expectedTimes)
            dict_chapter_act_time_seq[ch] = act_time_seq

        dict_user_chapter_actTimeSeq[u] = dict_chapter_act_time_seq


    dict_user_chapter_actTimeSeq = quantize_raw_elapsedTime_2(dict_user_chapter_actTimeSeq, dict_chapter_pdfID.keys())


    # for u in dict_user_chapter_actTimeSeq:
    #     print(u)
    #     for ch in dict_user_chapter_actTimeSeq[u]:
    #         for act in dict_user_chapter_actTimeSeq[u][ch]:
    #             print(act)
    #         print('-----')
    #     print ''


    with open('PBP_data2/dict_user_chapter_actTimeSeq_for_PDF_%s.json' %course_id, 'w') as fp:
        json.dump(dict_user_chapter_actTimeSeq, fp)

    return 0

########################################################################################################################








