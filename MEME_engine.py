import sys
import math
import numpy as np
import pandas as pd
import scipy
import random
import collections
import logging
import copy


class Mnode():
    def __init__(self, act):
        self.act = act
        self.count = 0
        self.depth = -1
        self.isEnd = False
        self.muted = False
        self.prob = 0 # only has value when isEnd == True
        self.kids = {} # a dictionary of child nodes


class Tree():
    def __init__(self, L):
        self.L = L
        self.root = Mnode('root')
        self.root.depth = 0
        self.theta = [] # letter-probability matrix
        self.best_combined_prob = 0
        self.best_motif = []
        self.best_count = 0
        self.max_count = 0
        self.result_motif = []

    def insert(self, seq): # put a subsequence into the tree
        node = self.root
        node.count += 1
        for act in seq:
            if act not in node.kids:
                node.kids[act] = Mnode(act)
            current_depth = node.depth
            node = node.kids.get(act)
            node.count += 1
            node.depth = current_depth + 1
        node.isEnd = True

    def check_count(self, seq):
        node = self.root
        for act in seq:
            if act not in node.kids:
                return False
            node = node.kids[act]
        return node.count

    def check_if_mute(self, seq):
        node = self.root
        for act in seq:
            if act not in node.kids:
                return 'input seq not exist'
            node = node.kids[act]
        return node.muted


    def mute(self, seq):
        # mute the end node of a seq
        node = self.root
        for act in seq:
            if act not in node.kids:
                return False
            node = node.kids[act]
        node.muted = True
        return True

    def mute_to_on(self, seq):
        # turn on the end node of a seq
        node = self.root
        for act in seq:
            if act not in node.kids:
                return False
            node = node.kids[act]
        node.muted = False
        return True


    def delete(self, seq):
        node = self.root
        # for act in seq:  #todo: need more thinking as an exercise


    def get_prob_from_theta(self, act, pos):
        index = self.L.index(act)
        return self.theta[pos][index]

    def find_max_prob_motif(self, theta):
        self.theta = theta
        self.best_combined_prob = 0
        node = self.root
        for act in node.kids:
            self.trace(node.kids[act], 1, [])
        return self.best_combined_prob, self.best_motif, self.best_count

    def trace(self, node, combined_prob, motif):
        p = self.get_prob_from_theta(node.act, node.depth)
        if node.isEnd and not node.muted:
            node.prob = combined_prob * p
            if node.prob > self.best_combined_prob:
                self.best_combined_prob, self.best_motif, self.best_count = node.prob, motif+[node.act], node.count
        else:
            for kid in node.kids:
                self.trace(node.kids[kid], combined_prob*p, motif+[node.act])

    def find_motif_max_count(self):
        self.max_count = 0
        node = self.root
        for act in node.kids:
            self.trace_for_count(node.kids[act], [])
        return self.result_motif, self.max_count

    def trace_for_count(self, node, motif):
        if node.isEnd and node.muted == False:
            if node.count > self.max_count:
                self.result_motif, self.max_count =  motif+[node.act], node.count
        else:
            for kid in node.kids:
                self.trace_for_count(node.kids[kid], motif + [node.act])


class MEME():

    def __init__(self, users, Y, L, prob_a=0.5, alpha = 0.1, beta = 1e-8, THRESH=1e-6):
        self.users = users
        self.Y = Y
        self.L = L # 1 by number_of_letters list containing the alphabet
        self.prob_a = prob_a # probability of matching letter in making the starting point letter probability matrix
        self.alpha = alpha
        self.beta = beta
        self.THRESH = THRESH

        LOG_FILENAME = "log.out"
        logging.basicConfig(filename=LOG_FILENAME)


    def calculate_letter_frequency(self):
        # count the frequency of letters in the dataset
        # freq is aligned with L, freq[i] is the number of occurances of L[i] in the dataset
        freq = np.zeros(len(self.L))
        for y in self.Y:
            for act in y:
                i = self.L.index(act)
                freq[i] += 1
        return freq


    def mapping_1d_index_in_2d_array(self, X_indexes):
        # 2d numpy smallz_index => in small z: ij-th position containing the index in Z1, if invalid ij, index = -1
        i_max = max([indexes[0] for indexes in X_indexes])
        j_max = max([indexes[1] for indexes in X_indexes])
        smallz_index = np.full((i_max+1, j_max+1), -1, dtype=np.int) # e.g. max index = 4, then length of dimension should be 5

        # mapping:
        for i, indexes in enumerate(X_indexes):
            smallz_index[indexes[0]][indexes[1]] = i

        num_row = i_max + 1
        num_col = j_max + 1
        return smallz_index, num_row, num_col


    def sequence_segmentation_and_erasingFactor(self, W):
        # Y is a list of lists, with each list being the behavior sequence of a user
        # W is an integer, length of motif
        X = []
        X_indexes = []
        for n, y in enumerate(self.Y):
            if len(y) < W:
                continue
            for i in range(0, len(y) - W + 1):
                tmp = y[i:i+W]
                X.append(tmp)
                X_indexes.append((n, i))

        return X, X_indexes


    def random_select_one(self, X):
        i = random.randint(0, len(X))
        x = X[i]
        return i, x


    def theta_from_x(self, x, f0):
        # e.g. x = motif, i.e. ['Pl', 'Pa', 'Sf']
        # theta is a list of lists, i-th list being the probability ditribution at the i-th position of motif; with 0th being background,
        # prob_a + prob_b * (length(L) - 1) = 1
        prob_b = (1-self.prob_a)/float(len(self.L) - 1)
        theta = [f0]
        for i, act in enumerate(x):
            prob_dist = []
            for tmp in self.L:
                if tmp == act:
                    prob_dist.append(self.prob_a)
                else:
                    prob_dist.append(prob_b)

            theta.append(prob_dist)
        return theta


    def compute_bigZ_for_subseq_ij(self, theta, lamda, W, subseq):
        # refer to equation 10 of the tech report
        # W is the subseq ij (ith row and jth position in Y)
        # theta: probability distribution matrix
        # compute Zi1 for the motif model
        # compute Zi2 for the background model
        prob_m, prob_b = 1.0, 1.0
        for pos in range(1, W+1): # for each position in the motif
            ind = self.L.index(subseq[pos-1])
            prob_m = prob_m * theta[pos][ind]  # todo: when init theta, need to add one column as 0th column for background
            prob_b = prob_b * theta[0][ind]
        Zi1 = lamda*prob_m / (lamda*prob_m + (1-lamda)*prob_b)

        return Zi1


    def EStep_update_smallz(self, num_row, num_col, theta, lamda, W):
        # this is the E step
        # Y is a list of lists, with each list being the behavior sequence of a user
        # W is the length of motif to find
        smallz1 = np.full((num_row, num_col), fill_value=-1)
        for i in range(0, len(self.Y)):
            seq = self.Y[i]
            if len(seq) < W:   #todo: remove short rows at the beginning
                # print('remove short rows at the beginning!')
                continue
            for j in range(0, len(seq) - W + 1):
                subseq = seq[j:j+W]
                smallz1[i][j] = self.compute_bigZ_for_subseq_ij(theta, lamda, W, subseq)

        return smallz1


    def MStep_estimate_new_lamda(self, Z1, num_all_subseq):
        new_lamda_1 = 0
        for x in Z1:
            new_lamda_1 += x
        new_lamda_1 /= float(num_all_subseq)
        return new_lamda_1


    def MStep_compute_Cjk_and_C0k(self, X, motif, Z1, E):
        # L = collection of all letters, or behaviors
        # E = erasing factor of all subseq
        C0k = np.zeros(len(self.L))
        Cjk = np.zeros((len(motif), len(self.L)))
        w = len(motif)
        # motif_str = '_'.join(motif) # to delete
        for k in range(0, len(self.L)): # for each letter in alphabet
            for j in range(0, w): # for each position in motif
                # compute c_jk; see eqn 17
                # iterate through all subseq
                for index, subseq in enumerate(X):
                    # try:
                        if index == -1:
                            continue
                        # compute c_jk
                        erasing_factor = E[index]
                        z_value = Z1[index]
                        # I(k, X_ij) = 1 if X_ij = l_k otherwise 0;  l_k is a_k, k-th letter in alphabet
                        I_value = 1 if X[index][j] == self.L[k] else 0
                        Cjk[j][k] += erasing_factor * z_value * I_value

                        # compute c_0k
                        C0k[k] += (1-z_value) * I_value
                    # except:
                    #     print('----- error 1 ------')
                    #     print(motif)
                    #     sys.exit("Error message")


        return C0k, Cjk


    def estimate_goodness_theta_lamda(self, lamda, theta, W, n):
        # see equation at the end of section 3
        goodness = 0
        lamda_background = 1 - lamda
        for k in range(0, len(self.L)):
            for i in range(1, W+1):
                goodness += lamda_background * theta[0][k] * math.log(theta[0][k])
                goodness += lamda * theta[i][k] * math.log(theta[0][k])
        goodness += lamda_background + math.log(lamda_background)
        goodness += lamda + math.log(lamda)
        goodness = goodness * n

        return goodness


    def MStep_compute_new_theta(self, C0k, Cjk, W, beta_vec):
        # beta = [b1, b2, ..., bk]
        # new theta = <f0, f1, f2, ..., fw>, with each f_j = f_jk for k = 1, ..., L_length;  L = list of all letters
        L_length = len(self.L)
        new_theta = np.zeros((W+1, L_length))

        # f_0
        sum_c0 = float(sum(C0k) + sum(beta_vec))
        for k in range(0, L_length):
            new_theta[0][k] = (C0k[k] + beta_vec[k]) / sum_c0
        # f_jk for j = 1,...,W
        for j in range(0, W):
            sum_cj = float(sum(Cjk[j][:]) + sum(beta_vec))
            for k in range(0, L_length):
                if Cjk[j][k] == 0:
                    new_theta[j + 1][k] = 0
                else:
                    new_theta[j+1][k] = (Cjk[j][k] + beta_vec[k]) / sum_cj

        return new_theta


    def get_motif_from_theta(self, theta, all_subseq_tree, discovered_motifs):
        # indexes = np.argmax(theta, axis=1) # Maxima along the first axis
        # motif = [L[int(i)] for i in indexes[1:]]
        this_tree = copy.deepcopy(all_subseq_tree)
        for m in discovered_motifs:
            this_tree.mute(m)
        motif_prob, motif, motif_count = this_tree.find_max_prob_motif(theta)
        return motif, motif_prob, motif_count


    def compare(self, subseq, motif):
        for i, item in enumerate(motif):
            if item != subseq[i]:
                return False
        return True


    def EM(self, n, X, E, motif, beta_vector, num_row, num_col, theta, lamda, W):
        # E
        # z1 for motif model, (1 - z1) for background model
        smallz1 = self.EStep_update_smallz(num_row, num_col, theta, lamda, W)

        # M
        Z1 = [x for x in smallz1.flatten() if x != -1]
        lamda = self.MStep_estimate_new_lamda(Z1, n)
        C0k, Cjk = self.MStep_compute_Cjk_and_C0k(X, motif, Z1, E)
        new_theta = self.MStep_compute_new_theta(C0k, Cjk, W, beta_vector)

        # calculate the change in theta
        d_theta = np.linalg.norm(new_theta - theta)

        return d_theta, new_theta, lamda

    def print_theta(self, best_theta):
        print(self.L)
        for i, row in enumerate(best_theta):
            print(row)
        return 0

    def run(self, W, NPasses):

        X, X_indexes = self.sequence_segmentation_and_erasingFactor(W)

        all_subseq_tree = Tree(self.L)
        for x in X:
            all_subseq_tree.insert(x)


        '''
        high_counts = []
        for i in range(30):
            m, count = all_subseq_tree.find_motif_max_count()
            all_subseq_tree.mute(m)
            high_counts.append(m)
            print(i, m, count)

        for m in high_counts:
            all_subseq_tree.mute_to_on(m)
        '''

        n = len(X)  # number of overlapping length-W subseq in entire dataset
        if len([indexes[0] for indexes in X_indexes]) == 0 or n < 10:
            return 0

        smallz_index, num_row, num_col = self.mapping_1d_index_in_2d_array(X_indexes)

        L_freq = self.calculate_letter_frequency()
        f0 = L_freq / float(sum(L_freq)) # background letter probability vector
        beta_vector = self.beta * L_freq # by definition at the end of 4.1 in tech report

        # N = len(self.Y)  # number of seq in dataset
        # start_value = 0
        # end_value = math.sqrt(N) / float(n)
        # step_size = 2/float(W)

        # set initial erasing factor
        E = np.ones(len(X))

        discovered_motifs = []
        for i in range(NPasses):
            best_theta = []
            best_goodness = -1e8
            best_lamda = 0
            for lamda in [0.1, 0.3, 0.5, 0.7]: #np.arange(0.1,0.9,0.1):
                # Best starting point:
                # Run EM for one or two on each subsequence and choose the one that yields the highest likelihood and
                # then run EM to convergence from this starting point
                max_goodness = -1e10
                motif, theta = [], []
                for i, x in enumerate(X):
                    if E[i] == 0:
                        continue
                    count = 0
                    theta_tmp = self.theta_from_x(x, f0)
                    while count < 2:
                        _, theta_tmp, lamda_new = self.EM(n, X, E, x, beta_vector, num_row, num_col, theta_tmp, lamda, W)
                        count += 1

                    goodness = self.estimate_goodness_theta_lamda(lamda, theta_tmp, W, n)
                    if goodness > max_goodness:
                        max_goodness, theta, motif = goodness, theta_tmp, x

                # # method 2: todo: replace
                # Q = math.log(1 - alpha) / math.log(1 - lamda)
                # max_goodness = 0
                # theta = 0
                # motif = []
                # for j in range(0, Q):
                #     index, motif_tmp = random_select_one(X)
                #     theta_tmp = theta_from_x(motif_tmp, L, prob_a)


                # Run MM from the starting point
                thresh = 10000
                count = 0
                while thresh > self.THRESH and count < 1000:
                    thresh, theta, lamda = self.EM(n, X, E, motif, beta_vector, num_row, num_col, theta, lamda, W)
                    count += 1

                goodness = self.estimate_goodness_theta_lamda(lamda, theta, W, n)
                if goodness > best_goodness:
                    best_goodness, best_theta, best_lamda = goodness, theta, lamda


            # self.print_theta(best_theta)

            motif, motif_prob, motif_count = self.get_motif_from_theta(best_theta, all_subseq_tree, discovered_motifs)
            discovered_motifs.append(motif)
            print('lambda = %s   motif = %s,  goodness = %s, prob = %s, count = %s'
                  %(best_lamda, '-->'.join(motif), best_goodness, motif_prob, motif_count))
            # logging.exception('\n\n%s did not go through. A report failed to generate for this course' % course)

            # update erazing factor
            for i, subseq in enumerate(X):
                if self.compare(subseq, motif):
                    E[i] = 0

        return 0


