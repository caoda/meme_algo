__author__ = 'dacao'

import sys
import json
import csv
import datetime
import unicodedata
import operator
import numpy
import pymongo

# import matplotlib
# import matplotlib.pyplot as plt
# from matplotlib.pyplot import plot, draw, show


import functions as func
import quantizing
from quantizing import quantize_action_elapsed_times_based_on_duration as quantize
# import boxplots

m_video = "VideoMeasurement"
m_pdf = 'PDFMeasurement'
m_lifecycle = 'LifeCycleMeasurement'
m_article = 'ArticleViewPositionMeasurement'
m_slide = 'SlidePlaybackEventMeasurement'
m_window = 'WindowLayoutMeasurement'
m_navi = 'NavigationMeasurement'
m_slideChanged = 'SlideChangedMeasurement'
m_slideCompleted = 'SlideCompletedMeasurement'



DB_name = 'prod_backend'

options = {
             'NAME': 'prod_backend',
             'HOST': 'ec2-34-197-131-74.compute-1.amazonaws.com', #,ec2-34-192-210-175.compute-1.amazonaws.com,ec2-34-197-47-228.compute-1.amazonaws.com'
             'PORT': 27017,
             'USER': 'prod',
             'PASSWORD': '|CtNtXK!D46vKXB',
             'REPLICASET': 'prod-mongo-rs-raw',
             'CONNECTION': None
        }

def do_connect_mongo(options):

    if options['REPLICASET'] == "":
        connection = pymongo.MongoClient(options['HOST'], options['PORT'])
    else:
        connection = pymongo.MongoClient(options['HOST'], options['PORT'], replicaset=options['REPLICASET'])
    connection[options['NAME']].authenticate(options['USER'], options['PASSWORD'])

    return connection


# parameters
Thres_NumberRepetition = 2
max_sequence_gap_time = 10*60
Thres_HF = 2.0 # seconds.  for scrubbs within this threshold, we don't consider it as final. e.g. user is just searching in the video
THRES_GAP = 2.5 # if timestamps of playback events are 3 * slide_duration apart, then treat them as separate sequences
Thres_close_events = 0.5 # seconds
Thres_short_play = 1.0 # seconds. If a play happens right after a scrubb and is shorter than this. Exclude this Play.



'''/////////////////////////////////////////////////////////////////////////////////////////////////////////////////'''


# def get_slide_title(slideID, chapterID, events):
#
#     for evt in events:
#         if 'chapter' in evt and evt['chapter'] == chapterID:
#             if 'slide' in evt and evt['slide'] == slideID:
#                 slide_title = evt['slideTitle']
#                 break
#
#     return slide_title



class action():

    def __init__(self, from_pos, to_pos, from_time, to_time, type, slide_id, duration, chapter):
        self.from_pos = from_pos
        self.to_pos = to_pos
        self.from_time = from_time
        self.to_time = to_time
        self.type = type
        self.slide_id = slide_id
        self.duration = duration
        self.chapter = chapter

    def traveled_video_length(self):

        if self.type == 'Play' or self.type == 'Scrubb':
            return (self.to_pos - self.from_pos) * self.duration
        else:
            return 0

    def time_elapsed(self):
        return self.to_time - self.from_time





def filter_out_scrubb_and_window_events(list_of_evts):
    new = []
    for evt in list_of_evts:
        if 'type' in evt and evt['type'] == 'Scrubb':
            continue
        elif evt['m_type'] == m_window:
            continue
        else:
            new.append(evt)

    return new



def handle_scrubbs_and_high_frequency_actions(action_list, Thres_HF, Thres_close_events, Thres_short_play, to_print):

    if len(action_list) < 3:
        return action_list

    # delete the pauses before scrubb if timestamp almost the same
    new = []
    for i, act in enumerate(action_list):
        if i == 0 or i == (len(action_list) - 1):
            new.append(act)
            continue

        next_act = action_list[i+1]
        if act.type == 'Pause' and next_act.type == 'Scrubb':
            if too_close(act.from_time, next_act.from_time, Thres_close_events):
                if to_print:
                    print('deleted', act.type, act.from_pos, act.to_pos, act.from_time)
            else:
                new.append(act)
        else:
            new.append(act)
    action_list = new


    # delete the Play after the Scrubb if timestamp almost the same and played video is too short.
    new = []
    for i, act in enumerate(action_list):
        if i == 0:
            new.append(act)
            continue

        prev_act = action_list[i-1]
        if act.type == 'Play' and prev_act.type == 'Scrubb':
            if too_close(act.from_time, prev_act.from_time, Thres_close_events) and act.time_elapsed() < Thres_short_play:
                if to_print:
                    print('deleted', act.type, act.from_pos, act.to_pos, act.from_time)
            else:
                new.append(act)
        else:
            new.append(act)
    action_list = new


    # combine those continuous scrubbs that are very close to each other. iterate thru and combine step by step
    new = []
    for i, act in enumerate(action_list):

        if i == 0 or i == len(action_list) - 1: # do nothing for the last act
            new.append(act)
            continue

        if act.type == 'Scrubb' and new[-1].type == 'Scrubb':
            if too_close(act.from_time, new[-1].from_time, Thres_HF):
                new[-1].to_pos = act.to_pos
                new[-1].from_time = act.from_time
            else:
                new.append(act)
        else:
            new.append(act)


    action_list = new
    del new

    return action_list



def too_close(val1, val2, Thres):
    if abs(val1 - val2) < Thres:
        return True
    else:
        return False



def good_scrubb(i, scrubb_evt, evts_list):

    s_time = scrubb_evt['timestamp']
    s_pos = scrubb_evt['position']
    s_toPos = scrubb_evt['toPosition']

    pause_evt = 'na'
    play_evt = 'na'

    start = max(0, i-2)
    last = min( len(evts_list)-1 , i+2 )

    for index in range(start, last+1):
        evt = evts_list[index]
        if 'type' in evt and evt['type'] == 'Pause' and too_close(evt['timestamp'], s_time, 1.0) and too_close(evt['position'], s_pos, 0.09):
            pause_evt = evt
        elif 'type' in evt and evt['type'] == 'Play' and too_close(evt['timestamp'], s_time, 1.0) and too_close(evt['position'], s_toPos, 0.09):
            play_evt = evt

    if pause_evt != 'na' and play_evt != 'na':
        # times = sorted([pause_evt['timestamp'], play_evt['timestamp'], s_time])
        init = min(pause_evt['timestamp'], play_evt['timestamp'], s_time)
        pause_evt['timestamp'] = init
        scrubb_evt['timestamp'] = init + 0.01
        play_evt['timestamp'] = init + 0.02
    elif pause_evt != 'na' and play_evt == 'na':
        init = min(pause_evt['timestamp'], s_time)
        pause_evt['timestamp'] = init
        scrubb_evt['timestamp'] = init + 0.01

    return evts_list


def remove_invalid_scrubbs(evts_list):

    new = []
    for evt in evts_list:
        if 'pop' in evt:
            continue
        else:
            new.append(evt)

    evts_list = new
    del new
    return evts_list



def handle_scrubb_inconsistency(evts_list):

    for i, evt in enumerate(evts_list):
        if 'type' in evt and evt['type'] == 'Scrubb':
            evts_list = good_scrubb(i, evt, evts_list)

    evts_list = sorted(evts_list, key= lambda k:k['timestamp'])

    return evts_list


def divide_list_of_events_based_on_discontinuity(slideID, slideDur, evts_list):

    all_clicktream_event_series = []

    ref_type = 'idle'  # 'Play', 'Pause'
    action_series = []
    for i, evt in enumerate(evts_list):
        if evt['m_type'] == m_slide:

            if ref_type == 'idle':
                action_series.append(evt)
                ref_time = evt['timestamp']
                ref_type = evt['type']

            else:
                if (evt['timestamp'] - ref_time) > slideDur * THRES_GAP:
                    all_clicktream_event_series.append( action_series )
                    ref_time = evt['timestamp']
                    ref_type = evt['type']
                    action_series = [evt]
                else:
                    action_series.append(evt)
                    ref_time = evt['timestamp']
                    ref_type = evt['type']

        elif evt['m_type'] == m_navi and evt['action'] in ['open', 'close']:
            action_series.append(evt)
            all_clicktream_event_series.append( action_series )
            ref_type = 'idle'
            action_series = []
        elif evt['m_type'] in [m_slideChanged, m_slideCompleted]:
            action_series.append(evt)
            all_clicktream_event_series.append( action_series )
            ref_type = 'idle'
            action_series = []

    if len(action_series) > 0:
        all_clicktream_event_series.append( action_series )

    return all_clicktream_event_series




def deal_with_Replay(sequence, slideID, slideDur):
    new = []
    for i, act in enumerate(sequence):
        if act.type == 'Replay':
            if i == 0:
                act.type = 'Play'
                new.append(act)
            else:
                chapter = act.chapter
                tmp = action(sequence[i-1].to_pos, 0.0, act.from_time, act.from_time, 'Scrubb', slideID, slideDur, chapter)
                new.append(tmp)
                act.type = 'Play'
                new.append(act)

        else:
            new.append(act)

    return new

def compute_raw_action_list_of_single_slide(slideID, slideDur, events, user, to_print):

    all_clicktream_event_series = divide_list_of_events_based_on_discontinuity(slideID, slideDur, events)

    all_action_sequences = []

    for evts_list in all_clicktream_event_series:
        sequence = []
        ref_type = 'idle'  # 'Play', 'Pause'

        evts_list = handle_scrubb_inconsistency(evts_list)

        if to_print:
            print('---- clickstream after handling scrubb inconsistency ------')
            for x in evts_list:
                print(x['m_type'], x.get('duration'), x.get('slide'), x.get('type'), x.get('timestamp'), x.get('position'), x.get('toPosition'))
            print('-----------\n')

        for i, evt in enumerate(evts_list):

            if evt['m_type'] == m_slide and evt['type'] in ['Pause', 'Play', 'Replay', 'Scrubb']:

                if ref_type == 'idle':
                    ref_pos = evt['position']
                    if evt['type'] == 'Scrubb':
                        ref_to_pos = evt['toPosition']
                    else:
                        ref_to_pos = 0
                    ref_time = evt['timestamp']
                    ref_type = evt['type']
                else:
                    new_pos = evt['position']
                    new_time = evt['timestamp']
                    chapter = evt.get('chapter', 'none')
                    if ref_type == 'Scrubb':
                        new_act = action(ref_pos, ref_to_pos, ref_time, new_time, ref_type, slideID, slideDur, chapter)
                        sequence.append(new_act)
                    elif ref_type == 'Pause':
                        new_act = action(ref_pos, ref_pos, ref_time, new_time, ref_type, slideID, slideDur, chapter)
                        sequence.append(new_act)
                    else:
                        new_act = action(ref_pos, new_pos, ref_time, new_time, ref_type, slideID, slideDur, chapter)
                        sequence.append(new_act)

                    ref_pos = evt['position']
                    if evt['type'] == 'Scrubb':
                        ref_to_pos = evt['toPosition']
                    else:
                        ref_to_pos = 0
                    ref_time = evt['timestamp']
                    ref_type = evt['type']

            elif i == len(evts_list) - 1 and ref_type != 'idle':
                if evt.get('m_type') in [m_slideCompleted, m_slideChanged, m_navi]:
                    new_time = evt['timestamp']
                    chapter = evt.get('chapter', 'none')
                    if ref_type == 'Play':
                        new_pos = min( (ref_pos*slideDur + new_time-ref_time)/float(slideDur), 1.0)
                        new_act = action(ref_pos, new_pos, ref_time, new_time, ref_type, slideID, slideDur, chapter)
                        sequence.append(new_act)

        # deal with Replay:  insert a scrubb and change Replay to play
        sequence = deal_with_Replay(sequence, slideID, slideDur)

        if to_print:
            print('---- raw action sequence ----')
            for act in sequence:
                print(act.type, act.from_pos, act.to_pos, act.from_time)
            print('------\n')


        all_action_sequences.append(sequence)

    return all_action_sequences





def divide_evts_in_chapter_into_segments_based_on_slides_for_single_user(list_of_evts):

    result = {}

    current_slide_id = 'fake'
    tmp = []
    for i, evt in enumerate(list_of_evts):
        if current_slide_id == 'fake' and evt['m_type'] == m_slide and evt['slide'] != current_slide_id:
            tmp.append(evt)
            current_slide_id = evt['slide']
            result[current_slide_id] = []

        elif current_slide_id != 'fake':
            if evt['m_type'] == m_slide and evt['slide'] != current_slide_id:
                result[current_slide_id] += tmp
                tmp = [evt]
                current_slide_id = evt['slide']
                result[current_slide_id] = []
            else:
                tmp.append(evt)

    if current_slide_id != 'fake' and len(tmp) > 0:
        result[current_slide_id] += tmp

    return result


def filter_out_successive_Pl_Pa(actions):

    new = []
    for i, item in enumerate(actions):
        if i > 0 and item['type'] == 'Pause' and actions[i-1]['type'] == 'Pause':
            continue
        elif i > 0 and item['type'] == 'Play' and actions[i-1]['type'] == 'Play':
            continue

        new.append(item)

    actions = new
    del new
    return actions


def prepare_for_main(course_id):

    CONNECTION = do_connect_mongo(options)

    collection_raw_events = CONNECTION[DB_name]['measure_base']
    events = list(  collection_raw_events.find( {"course": course_id} ).sort('timestamp')  )

    slideIDs_durations = {}
    for evt in events:
        if 'slide' in evt and 'duration' in evt:
            sID = evt['slide']
            dura = evt['duration']
            if sID not in slideIDs_durations:
                slideIDs_durations[ sID ] = { dura :1 }
            else:
                if dura not in slideIDs_durations[ sID ]:
                    slideIDs_durations[sID][dura] = 1
                else:
                    slideIDs_durations[sID][dura] += 1

    tmp = {}
    for id in slideIDs_durations:
        print(slideIDs_durations)
        durs = slideIDs_durations[id]
        # du = max(].iteritems(), key=operator.itemgetter(1))[0]
        du = max(durs.iterkeys(), key=(lambda key: durs[key]))
        tmp[id] = float(du)

    slideIDs_durations = tmp
    del tmp

    chap_names = []
    for evt in events:
        if 'chapter' in evt and evt['chapter'] not in chap_names:
            chap_names.append(evt['chapter'])
    chap_names = sorted(chap_names)

    return events, chap_names, slideIDs_durations




def main(courseID, print_debug, debug_slide_id, debug_user):

    events, chap_names, slideIDs_durations = prepare_for_main(courseID)

    uv = func.get_uv_dict(events)
    uv = func.make_valid(uv)
    users = sorted(uv.keys())

    actions_dict_user_slideID_sequences = {}

    for user in users:

        print(users)

        actions_dict_user_slideID_sequences[user] = {}

        all_evts_this_user = uv[user]
        chapters, all_lc = func.get_chapEvt_dict_and_lifeCycle(all_evts_this_user)

        for ch in chap_names:
            if ch not in chapters:
                continue

            evts_list = chapters[ch]

            if print_debug:
                if user == debug_user:
                    print('---- raw clickstream data ----')
                    for x in evts_list:
                        #if x.get('slide') == debug_slide_id:
                        print(x['m_type'],x.get('duration'), x.get('slide'), x.get('type'), x.get('timestamp'), x.get('position'), x.get('toPosition'), x.get('type'))
                    print('---- end ----\n')

            slide_evt_segments = divide_evts_in_chapter_into_segments_based_on_slides_for_single_user(evts_list)

            for sID in slide_evt_segments:

                if sID == debug_slide_id and user == debug_user:
                    to_print = True
                else:
                    to_print = False

                actions_dict_user_slideID_sequences[user][sID] = []

                all_action_sequences = compute_raw_action_list_of_single_slide(sID, slideIDs_durations[sID], slide_evt_segments[sID], user, to_print)

                for sequence in all_action_sequences:

                    sequence = handle_scrubbs_and_high_frequency_actions(sequence, Thres_HF, Thres_close_events, Thres_short_play, to_print)

                    actions = []
                    for act in sequence:

                        tmp = {'type':act.type, 'time_elapsed': 0.0, 'position': act.from_pos, 'timestamp': act.to_time, 'chapter': act.chapter,
                               'slideID':act.slide_id}
                        if act.type == 'Pause':
                            tmp['time_elapsed'] = act.time_elapsed()
                        else:
                            tmp['time_elapsed'] = act.traveled_video_length()

                        if tmp['type'] == 'Scrubb' and tmp['time_elapsed'] >= 0:
                            tmp['type'] = 'Sf'
                        elif tmp['type'] == 'Scrubb' and tmp['time_elapsed'] < 0:
                            tmp['type'] = 'Sb'
                            tmp['time_elapsed'] = abs( tmp['time_elapsed'] )

                        actions.append(tmp)

                    if to_print:
                        print('--- final action sequence---')
                        for a in actions:
                            print(a)
                        print('---end---')

                    actions = filter_out_successive_Pl_Pa(actions)

                    actions_dict_user_slideID_sequences[user][sID].append(actions)


    actions_dict_user_slideID_sequences = quantize(actions_dict_user_slideID_sequences, slideIDs_durations)


    for user in actions_dict_user_slideID_sequences:
        for sID in actions_dict_user_slideID_sequences[user]:
            for acts in actions_dict_user_slideID_sequences[user][sID]:
                for act in acts:
                    if act['type'] == 'Play':
                        act['type'] = 'Pl'
                    elif act['type'] == 'Pause':
                        act['type'] = 'Pa'

    for user in actions_dict_user_slideID_sequences:
        for sID in actions_dict_user_slideID_sequences[user]:
            for acts in actions_dict_user_slideID_sequences[user][sID]:
               # if len(acts) > 2:
                if sID == debug_slide_id and user == debug_user:
                        # print('\n\nRESULT: ')
                        print(sID, user)
                        for act in acts:
                            print('%s   %s' %(act['type'], act['time_elapsed']))
                        print('--------')


    with open('PBP_data2/users_slideIDs_sequences_%s.json' %courseID, 'w') as fp:
        json.dump(actions_dict_user_slideID_sequences, fp)

    return 0

########################################################################################################################


debug = False
debug_slide_id = u''
debug_user = u''

courseID = 'PBP_WLC_305_2'

main(courseID, debug, debug_slide_id, debug_user)








