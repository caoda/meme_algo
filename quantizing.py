import numpy as np


# todo: make the number of quantiles a variable in future
# def quantize_action_elapsed_times_based_on_other_users(actions_dict_user_slideID_sequences, slideIDs_durations):
#
#     slideIDs = slideIDs_durations.keys()
#
#     slideID_acts = {}
#     for sID in slideIDs:
#         slideID_acts[sID] = []
#         for u in actions_dict_user_slideID_sequences:
#             if sID in actions_dict_user_slideID_sequences[u]:
#                 all_seq = actions_dict_user_slideID_sequences[u][sID]
#                 for seq in all_seq:
#                     for act in seq:
#                         slideID_acts[sID].append(act)
#
#     slideID_quan = {}
#     for sID in slideID_acts:
#         slideID_quan[sID] = {'Play':{}, 'Pause':{}, 'Scrubb':{}}
#         elapsed_times_play = []
#         elapsed_times_pause = []
#         elapsed_times_scrubb = []
#         for act in slideID_acts[sID]:
#             if act['type'] == 'Play':
#                 elapsed_times_play.append(act['time_elapsed'])
#             if act['type'] == 'Pause':
#                 elapsed_times_pause.append(act['time_elapsed'])
#             if act['type'] == 'Scrubb':
#                 elapsed_times_scrubb.append(abs(act['time_elapsed']))
#
#         elapsed_times_play = np.array(elapsed_times_play)
#         elapsed_times_pause = np.array(elapsed_times_pause)
#         elapsed_times_scrubb = np.array(elapsed_times_scrubb)
#
#         if len(elapsed_times_play) > 0:
#             slideID_quan[sID]['Play']['quan_1'] = np.percentile(elapsed_times_play, 25)
#             slideID_quan[sID]['Play']['quan_2'] = np.percentile(elapsed_times_play, 50)
#             slideID_quan[sID]['Play']['quan_3'] = np.percentile(elapsed_times_play, 75)
#
#         if len(elapsed_times_pause) > 0:
#             slideID_quan[sID]['Pause']['quan_1'] = np.percentile(elapsed_times_pause, 25)
#             slideID_quan[sID]['Pause']['quan_2'] = np.percentile(elapsed_times_pause, 50)
#             slideID_quan[sID]['Pause']['quan_3'] = np.percentile(elapsed_times_pause, 75)
#
#         if len(elapsed_times_scrubb) > 0:
#             slideID_quan[sID]['Scrubb']['quan_1'] = np.percentile(elapsed_times_scrubb, 25)
#             slideID_quan[sID]['Scrubb']['quan_2'] = np.percentile(elapsed_times_scrubb, 50)
#             slideID_quan[sID]['Scrubb']['quan_3'] = np.percentile(elapsed_times_scrubb, 75)
#
#
#     for u in actions_dict_user_slideID_sequences:
#         for sID in actions_dict_user_slideID_sequences[u]:
#             for seq in actions_dict_user_slideID_sequences[u][sID]:
#                 for act in seq:
#
#                     q1 = slideID_quan[sID][act['type']]['quan_1']
#                     q2 = slideID_quan[sID][act['type']]['quan_2']
#                     q3 = slideID_quan[sID][act['type']]['quan_3']
#
#                     if act['time_elapsed'] < q1:
#                         act['time_elapsed'] = 1
#                     elif q1 <= act['time_elapsed'] < q2:
#                         act['time_elapsed'] = 2
#                     elif q2 <= act['time_elapsed'] < q3:
#                         act['time_elapsed'] = 3
#                     else:
#                         act['time_elapsed'] = 4
#
#     return actions_dict_user_slideID_sequences



# def quantize_action_elapsed_times_based_on_duration(actions_dict_user_slideID_sequences, slideIDs_durations):
#
#     for u in actions_dict_user_slideID_sequences:
#         for sID in actions_dict_user_slideID_sequences[u]:
#
#             q1 = 0.5 * slideIDs_durations[sID]
#             q2 = 1.0 * slideIDs_durations[sID]
#             q3 = 2.0 * slideIDs_durations[sID]
#
#             for seq in actions_dict_user_slideID_sequences[u][sID]:
#                 for act in seq:
#
#                     if act['time_elapsed'] < q1:
#                         act['time_elapsed'] = 1
#                     elif q1 <= act['time_elapsed'] < q2:
#                         act['time_elapsed'] = 2
#                     elif q2 <= act['time_elapsed'] < q3:
#                         act['time_elapsed'] = 3
#                     else:
#                         act['time_elapsed'] = 4
#
#     return actions_dict_user_slideID_sequences


def quantize_action_elapsed_times_based_on_duration(actions_dict_user_slideID_sequences, slideIDs_durations):

    for u in actions_dict_user_slideID_sequences:
        for sID in actions_dict_user_slideID_sequences[u]:

            q = 0.7 * slideIDs_durations[sID]

            for seq in actions_dict_user_slideID_sequences[u][sID]:
                for act in seq:

                    if act['time_elapsed'] <= q:
                        act['time_elapsed'] = 1
                    else:
                        act['time_elapsed'] = 2

    return actions_dict_user_slideID_sequences