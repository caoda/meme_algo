__author__ = 'dacao'

import sys
import json
import csv
import datetime
import unicodedata
import operator
import numpy
import copy
import pymongo
import logging
import MEME_engine


m_video = "VideoMeasurement"
m_pdf = 'PDFMeasurement'
m_lifecycle = 'LifeCycleMeasurement'
m_article = 'ArticleViewPositionMeasurement'
m_slide = 'SlidePlaybackEventMeasurement'
m_window = 'WindowLayoutMeasurement'
m_navi = 'NavigationMeasurement'
m_slideChanged = 'SlideChangedMeasurement'
m_slideCompleted = 'SlideCompletedMeasurement'



DB_name = 'prod_backend'

options = {
             'NAME': 'prod_backend',
             'HOST': 'ec2-34-197-131-74.compute-1.amazonaws.com', #,ec2-34-192-210-175.compute-1.amazonaws.com,ec2-34-197-47-228.compute-1.amazonaws.com'
             'PORT': 27017,
             'USER': 'prod',
             'PASSWORD': '|CtNtXK!D46vKXB',
             'REPLICASET': 'prod-mongo-rs-raw',
             'CONNECTION': None
        }

def do_connect_mongo(options):

    if options['REPLICASET'] == "":
        connection = pymongo.MongoClient(options['HOST'], options['PORT'])
    else:
        connection = pymongo.MongoClient(options['HOST'], options['PORT'], replicaset=options['REPLICASET'])
    connection[options['NAME']].authenticate(options['USER'], options['PASSWORD'])

    return connection


CONNECTION = do_connect_mongo(options)
collection_raw_events = CONNECTION[DB_name]['measure_base']



def prepare():
    # courses = list(collection_raw_events.distinct('course'))
    # with open('courses.txt', 'w') as outfile:
    #     json.dump(courses, outfile)

    with open('courses.txt') as outfile:
        courses = json.load(outfile)


    q_measure = 'QuizMeasurement'
    q_answer = 'QuizAnswerMeasurement'

    pmi_courses_with_Q = []
    # pmi_courses_500_up = []
    # pmi_courses_300_500 = []
    # pmi_courses_100_300 = []
    pmi_courses_200_down = []
    for c in courses:
        try:
            # users_answer = list(collection_raw_events.find({'course':c, 'm_type':q_answer}).distinct('user'))
            # users_q = list(collection_raw_events.find({'course': c, 'm_type': q_measure}).distinct('user'))
            # types = list(collection_raw_events.find({'course': c}).distinct('m_type'))
            users = list(collection_raw_events.find({'course': c}).distinct('user'))

            # print(c, types)
            # if len(users_answer) > 1 or len(users_q) > 1:
            #     print(c, len(users_answer), len(users_q))
            #     pmi_courses_with_Q.append(c)

            if len(users) < 200:
                print(c, len(users))
                pmi_courses_200_down.append(c)

            # if len(users) > 500:
            #     print(c, len(users))
            #     pmi_courses_500_up.append(c)
            # if 300 < len(users) < 500:
            #     print(c, len(users))
            #     pmi_courses_300_500.append(c)
            # if 100 < len(users) < 300:
            #     print(c, len(users))
            #     pmi_courses_100_300.append(c)

        except:
            logging.exception('%s failed' %c)
            continue

    # with open('pmi_courses_500_up.txt', 'w') as outfile:
    #     json.dump(pmi_courses_500_up, outfile)
    #
    # with open('pmi_courses_300_500.txt', 'w') as outfile:
    #     json.dump(pmi_courses_300_500, outfile)
    #
    # with open('pmi_courses_100_300.txt', 'w') as outfile:
    #     json.dump(pmi_courses_100_300, outfile)

    with open('pmi_courses_200_down.json', 'w') as outfile:
        json.dump(pmi_courses_200_down, outfile)


    return 0




'''
LifeCycleMeasurement
NavigationMeasurement
WindowLayoutMeasurement
SlidePlaybackEventMeasurement
SlideCompletedMeasurement
SlideChangedMeasurement
SlideShowLayerMeasurement
SlideLayerCompletedMeasurement
'''

def get_u_seq(course):

    # m_type = list(collection_raw_events.find({'course': course}).distinct('m_type'))
    # for t in m_type:
    #     print(t)

    # slideIndexes = collection_raw_events.find({'course': 'ITM_EU_CH_OP_001_1482343500000', 'm_type': 'SlidePlaybackEventMeasurement'}).distinct('slideIndex')
    # slideIndexes.sort()
    # print(slideIndexes)

    # actions = collection_raw_events.find(
    #     {'course': 'ITM_EU_CH_OP_001_1482343500000', 'm_type': 'SlidePlaybackEventMeasurement'}).distinct('type')
    # print(actions)

    users = list(collection_raw_events.find({'course': course}).distinct('user'))
    if len(users) > 150:
        return -1
    chap_names = ['init'] + sorted( list(collection_raw_events.find({'course': course}).distinct('chapter')) )

    u_seq = {}
    for u in users:
        events = list(collection_raw_events.find({'course': course, 'user':u}))

        actionSequences = []

        ref_slideIndex = -1
        ref_chap = 'init'
        just_did_lc = False

        for i, e in enumerate(events):
            chap = e.get('chapter', 'none')
            slideIndex = e.get('slideIndex', -1)
            m_type = e.get('m_type', 'none')
            t = e['timestamp']

            if chap != "none" and chap != ref_chap:
                new_ind = chap_names.index(chap)
                old_ind = chap_names.index(ref_chap)
                if new_ind - old_ind > 0:
                    act = 'chapF'  # +  str(int(new_ind-old_ind))
                else:
                    act = 'chapB'
                # act = 'chap' + str(int(new_ind-old_ind))
                actionSequences.append({'chapter':chap, 'timestamp':t, 'action':act})
                ref_chap = chap


            if slideIndex != ref_slideIndex:
                if ref_slideIndex == -1 or slideIndex == -1:
                    ref_slideIndex = slideIndex
                else:
                    if slideIndex > ref_slideIndex:
                        act = 'SlideF'
                    else:
                        act = 'SlideB'
                    actionSequences.append({'chapter':chap, 'timestamp':t, 'action':act, 'slideIndex':slideIndex, 'slideID':e['slide']})
                    ref_slideIndex = slideIndex

        new = []
        for i, act in enumerate(actionSequences):
            new.append(act)
            sInd = act.get('slideIndex', -1)
            if sInd != -1 and i < len(actionSequences)-1 and actionSequences[i+1].get('slideIndex', -1) != sInd:
                timespent = actionSequences[i+1].get('timestamp') - act.get('timestamp')
                tmp = copy.deepcopy(act)
                tmp['timespent'] = timespent
                tmp['action'] = 'SRead1'
                if timespent <= 15:
                    tmp['action'] = 'SRead1'
                else:
                    tmp['action'] = 'SRead2'

                new.append(tmp)
        actionSequences = new
        del new

            # if m_type == m_lifecycle:
            #     # if e.get('action', 'none') == 'backgroundTab' and lc_state == 'foregroundTab':
            #     #     lc_state = 'backgroundTab'
            #     if e.get('action', 'none') == 'foregroundTab' and events[i-1].get('action', 'none') == 'backgroundTab':
            #         # lc_state = 'foregroundTab'
            #         t0 = events[i-1]['timestamp']
            #         if just_did_lc == True:
            #             actionSequences[-1]['out_time'] += t - t0
            #         else:
            #             actionSequences.append({'chapter':ref_chap, 'timestamp':t0, 'action':'W', 'out_time':t - t0})
            #         just_did_lc = True
            #
            # if e.get('action', 'none') not in ['foregroundTab', 'backgroundTab']:
            #     just_did_lc = False


        # for act in actionSequences:
        #     if act.get('out_time', 'none') != 'none':
        #         out_time = act.get('out_time')
        #         if out_time <= 15:
        #             act['action'] = 'W_1'
        #         elif out_time > 15:
        #             act['action'] = 'W_2'

        # for i, act in enumerate(actionSequences):
        #
        u_seq[u] = actionSequences

    return u_seq




def get_Y_from_full_sequence(u_seq, W):

    # get alphabet L
    Y = []
    L = []
    U = []
    for u in u_seq.keys():
        letters = u_seq[u]

        if len(letters) < W:
            continue

        # if len(letters) < 50:
        #     continue

        U.append(u)
        letters = sorted(letters, key=lambda k: k['timestamp'])
        # letters = [l['action'] for l in letters if l['chapter']==chap]
        letters = [l['action'] for l in letters]

        # print(u, len(letters))
        Y.append(letters)
        L += letters
    L = list(set(L))

    return Y, U, L


#########################################################################################################



filename = 'pmi_courses_100_300.txt'
with open(filename, 'r') as fp:
    courses = json.load(fp)


# c = 'ITM_EU_CH_OP_001_1482343500000'

for c in courses:
    if c[0:3] == 'MED':
        continue
    print(c)
    # if len(c) < len('ITM_EEMA_RU_HR_023'):
    #     continue
    u_seq = get_u_seq(c)
    if u_seq == -1:
        continue

    with open('u_seq_%s.json'%c, 'w') as outfile:
        json.dump(u_seq, outfile)

    filename = 'u_seq_%s.json'%c
    with open(filename, 'r') as fp:
        u_seq = json.load(fp)

    W = 5
    Y, U, L = get_Y_from_full_sequence(u_seq, W)
    print(c, L)
    engine = MEME_engine.MEME(U, Y, L)
    engine.run(W, 3)


