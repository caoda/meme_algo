import csv


def user_info_list(users_from_reporting_tool, print_row):

    csvfile = open(users_from_reporting_tool, 'rU')
    reader = csv.reader(csvfile, dialect='excel')

    user_id_list = []
    dict_uid_details = {}

    userInfo = []
    for i, row in enumerate(reader):
        givenName = row[0]

        if givenName == 'Tania ':
            row[0] = 'Tania'

        familyName = row[1]
        email = row[2]
        id = row[3]
        userInfo.append(row)
        if i != 0:
            user_id_list.append(id)
            dict_uid_details[id] = {'first': givenName, 'last': familyName, 'email': email}

        if print_row:
            print(row)

    if print_row:
        print("\nThere are %s users in %s" %(len(userInfo), users_from_reporting_tool))
        print('**********\n')
    else:
        print("\nThere are %s users in %s. \nExample:" %(len(userInfo), users_from_reporting_tool))
        print(userInfo[0])
        print(userInfo[1])
        print('**********\n')


    return userInfo, user_id_list, dict_uid_details


def user_pass_fail_info_list(pass_fail_extend_info, print_row):

    passInfo = []
    with open(pass_fail_extend_info) as f:
        content = f.readlines()
        # content = content.split('\n')

        for row in content:
            line = row.split('\t')
            line = [l.replace('\n', '') for l in line]

            passInfo.append(line)

            if print_row:
                print(line)

    if print_row:
        print("\nThere are %s users in %s" %(len(passInfo), pass_fail_extend_info))
        print('**********\n')
    else:
        print("\nThere are %s users in %s. \nExample:" %(len(passInfo), pass_fail_extend_info))
        print(passInfo[0])
        print(passInfo[1])
        print('**********\n')

    return passInfo



def get_pass(userID, userInfo, passInfo, keyword_list):
    for line in userInfo:
        if userID == line[3]:
            family_name = line[1]
            given_name = line[0]

            for row in passInfo:
                if family_name in row and given_name in row:
                    if any(i.lower() in keyword_list for i in row):
                        return 1

    return 0





def run(courseID):

    if courseID == 'PBP_WLC_305_2':
        users_from_reporting_tool = 'first_time_users_from_reporting_tool/first-time-users-pbp-pbp_wlc_305_2.csv'
        pass_fail_extend_info = "pass_info/WLC305_2_Pass.txt"


    if courseID == 'PBP_WLC_305_4':
        users_from_reporting_tool = 'first_time_users_from_reporting_tool/first-time-users-pbp-pbp_wlc_305_4.csv'
        pass_fail_extend_info = 'pass_info/WLC305_4_Pass.txt'


    if courseID == 'PBP_WLC_305_5':
        users_from_reporting_tool = 'first_time_users_from_reporting_tool/first-time-users-pbp-pbp_wlc_305_5.csv'
        pass_fail_extend_info = 'pass_info/WLC305_5_Pass.txt'


    if courseID == 'PBP_WLC_303_4':
        users_from_reporting_tool = 'first_time_users_from_reporting_tool/first-time-users-pbp-pbp_wlc_303_4.csv'
        pass_fail_extend_info = "pass_info/WLC303_4_Pass.txt"


    if courseID == 'PBP_WLC_303_5':
        users_from_reporting_tool = 'first_time_users_from_reporting_tool/first-time-users-pbp-pbp_wlc_303_5.csv'
        pass_fail_extend_info = "pass_info/WLC303_5_Pass.txt"


    if courseID == 'PBP_WLC_303_5_B':
        users_from_reporting_tool = 'first_time_users_from_reporting_tool/first-time-users-pbp-pbp_wlc_303_5_B.csv'
        pass_fail_extend_info = "pass_info/WLC303_5_B_Pass.txt"


    if courseID == 'PBP_WLC_302_4':
        users_from_reporting_tool = 'first_time_users_from_reporting_tool/first-time-users-pbp-pbp_wlc_302_4.csv'
        pass_fail_extend_info = "pass_info/WLC302_4_Pass.txt"


    if courseID == 'PBP_MAH_102_4':
        users_from_reporting_tool = 'first_time_users_from_reporting_tool/first-time-users-pbp-pbp_mah_102_4.csv'
        pass_fail_extend_info = "pass_info/MAH102_4_Pass.txt"


    if courseID == 'PBP_MAH_102_5':
        users_from_reporting_tool = 'first_time_users_from_reporting_tool/first-time-users-pbp-pbp_mah_102_5.csv'
        pass_fail_extend_info = "pass_info/MAH102_5_Pass.txt"


    if courseID == 'PBP_MAH_102_6':
        users_from_reporting_tool = 'first_time_users_from_reporting_tool/first-time-users-pbp-pbp_mah_102_6.csv'
        pass_fail_extend_info = "pass_info/MAH102_6_Pass.txt"


    if courseID == 'PBP_MAH_104_4':
        users_from_reporting_tool = 'first_time_users_from_reporting_tool/first-time-users-pbp-pbp_mah_104_4.csv'
        pass_fail_extend_info = "pass_info/MAH104_4_Pass.txt"


    if courseID == 'PBP_MAH_104_5':
        users_from_reporting_tool = 'first_time_users_from_reporting_tool/first-time-users-pbp-pbp_mah_104_5.csv'
        pass_fail_extend_info = "pass_info/MAH104_5_Pass.txt"


    if courseID == 'PBP_MAH_104_6':
        users_from_reporting_tool = 'first_time_users_from_reporting_tool/first-time-users-pbp-pbp_mah_104_6.csv'
        pass_fail_extend_info = "pass_info/MAH104_6_Pass.txt"


    if courseID == 'PBP_MAH_103_4':
        users_from_reporting_tool = 'first_time_users_from_reporting_tool/first-time-users-pbp-pbp_mah_103_4.csv'
        pass_fail_extend_info = "pass_info/MAH103_4_Pass.txt"


    print_user_info = True
    print_pass_info = True

    print_user_info = False
    print_pass_info = False

    # get the users list and pass/fail information
    userInfo, user_id_list, dict_uid_details = user_info_list(users_from_reporting_tool, print_user_info)
    passInfo = user_pass_fail_info_list(pass_fail_extend_info, print_pass_info)

    userIDs_passed = []
    for u in user_id_list:
        if get_pass(u, userInfo, passInfo, ['pass', 'PASS']):
            userIDs_passed.append(u)

    return userIDs_passed

