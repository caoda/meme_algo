import json
import os
import MEME_engine
import nav_and_window_sequences
import get_passed_users

dir = 'PBP_data'
file_list = []
for filename in os.listdir(dir):
    if filename.startswith('users_') and filename.endswith(".json"):
        # print(os.path.join(directory, filename))
        file_list.append(os.path.join(dir, filename))


def explore(filename, maxi):

    with open(filename, 'r') as fp:
        dict_users_slideIDs_sequences = json.load(fp)


    # get all slideIDs #todo: make simpler
    sIDs = []
    for u in dict_users_slideIDs_sequences:
        tmp = dict_users_slideIDs_sequences[u].keys()
        sIDs += tmp
    sIDs = list(set(sIDs))
    print(sIDs)

    # get alphabet L
    max = 0
    W = 3
    for sID in sIDs:
        Y = []
        users = []
        L = []
        for u in dict_users_slideIDs_sequences:

            seq = [act for x in dict_users_slideIDs_sequences[u].get(sID, []) for act in x]
            if len(seq) < W:
                continue
            letters = [act['type'] + '_' + str(act['time_elapsed']) for act in seq]
            users.append(u)
            Y.append(letters)
            L += letters
        L = list(set(L))


        if len(users)>maxi:
            maxi = len(users)
            print(filename, sID)
            print(len(users))
            print(L)
            print('  ')

    return maxi, sIDs


def get_Y(filename, slideID, W):

    with open(filename, 'r') as fp:
        dict_users_slideIDs_sequences = json.load(fp)

    # get alphabet L
    Y = []
    users = []
    L = []
    for u in dict_users_slideIDs_sequences:

        seq = [act for x in dict_users_slideIDs_sequences[u].get(slideID, []) for act in x]
        if len(seq) < W:
            continue
        letters = [act['type'] + '_' + str(act['time_elapsed']) for act in seq]
        users.append(u)
        Y.append(letters)
        L += letters
    L = list(set(L))

    return Y, users, L




def get_Y_from_full_sequence(users, dict_users_slideIDs_sequences, dict_user_chapter_actTimeSeq, dict_user_ActionSequences, W, chap):

    # get alphabet L
    Y = []
    L = []
    U = []
    for u in users:
        letters = []

        # video
        if u in dict_users_slideIDs_sequences:
            seq = [act for sID in dict_users_slideIDs_sequences[u] for act in dict_users_slideIDs_sequences[u][sID]]
            seq = [item for sublist in seq for item in sublist]
            for act in seq:
                item = {'action': act['type'] + '_' + str(act['time_elapsed']), 'timestamp':act['timestamp'], 'chapter':act['chapter']}
                letters.append( item )

        # pdf
        if u in dict_user_chapter_actTimeSeq:
            seq = [act for ch in dict_user_chapter_actTimeSeq[u] for act in dict_user_chapter_actTimeSeq[u][ch]]
            # seq = [act for act in dict_user_chapter_actTimeSeq[u]['PBP_WLC_303_4_CHAPTER02']]
            # seq = [act for act in dict_user_chapter_actTimeSeq[u][chap]]
            for act in seq:
                if 'Sf' in act:
                    item = {'action': 'pdfSF' + '_' + str(int(act['Sf'])), 'timestamp':act['timestamp'], 'chapter':act['chapter']}
                elif 'Sb' in act:
                    item = {'action': 'pdfSB' + '_' + str(int(act['Sb'])), 'timestamp':act['timestamp'], 'chapter':act['chapter']}
                elif 'Re' in act:
                    item = {'action': 'pdfRE' + '_' + str(int(act['Re'])), 'timestamp':act['timestamp'], 'chapter':act['chapter']}
                letters.append(item)

        if u in dict_user_ActionSequences:
            letters += dict_user_ActionSequences[u]

        if len(letters) < W:
            continue

        # if len(letters) < 50:
        #     continue

        U.append(u)
        letters = sorted(letters, key=lambda k: k['timestamp'])
        letters = [l['action'] for l in letters if l['chapter']==chap]
        # letters = [l['action'] for l in letters]

        # print(u, len(letters))
        Y.append(letters)
        L += letters
    L = list(set(L))

    return Y, U, L


# filename = 'PBP_data/users_slideIDs_sequences_PBP_WLC_303_4.json'
# slideID = '6EfaX6tRNNj'


course1 = 'PBP_WLC_303_4'
course2 = 'PBP_WLC_305_2'
course3 = 'PBP_MAH_102_6'
course4 = 'PBP_MAH_102_5'
course5 = 'PBP_WLC_303_5'
course6 = 'PBP_MAH_102_4'



course = course5

filename = 'PBP_data2/users_slideIDs_sequences_' + course +'.json'
with open(filename, 'r') as fp:
    dict_users_slideIDs_sequences = json.load(fp)
filename = 'PBP_data2/dict_user_chapter_actTimeSeq_for_PDF_' + course + '.json'
with open(filename, 'r') as fp:
    dict_user_chapter_actTimeSeq = json.load(fp)

# dict_user_ActionSequences, chap_names = nav_and_window_sequences.main(course)
dict_user_ActionSequences = {}



W = 3
users_passed = get_passed_users.run(courseID=course)
users = [u for u in dict_user_chapter_actTimeSeq if u not in users_passed]
# print('users passed: ', users)

for chap in [course + '_CHAPTER0' + str(i) for i in range(0,10)]:
# for chap in ['PBP_WLC_305_2_CHAPTER04']:
    print(chap)
    Y, U, L = get_Y_from_full_sequence(users, dict_users_slideIDs_sequences, dict_user_chapter_actTimeSeq, dict_user_ActionSequences, W, chap)
    print(L)
    engine = MEME_engine.MEME(U, Y, L)
    engine.run(W, 5)
    # break

# Y, U, L = get_Y_from_full_sequence(users, dict_users_slideIDs_sequences, dict_user_chapter_actTimeSeq,
#                                    dict_user_ActionSequences, W, 'PBP_WLC_303_4_CHAPTER06')
# print(L)
# engine = MEME_engine.MEME(U, Y, L)
# engine.run(W, 3)







